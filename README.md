## PumpProgrammer
`Narzędzie do programowania pompy dla Start2000M.`
----
#### Wybeiranie portu COM
>Program generuje listę wykrytych portów COM w systemie za pomocą której możemy wybrać interesujący nas COM Port.
>Z regóły program przy starcie sam wyszukuje portów COM ale można wyzwolić skanowanie za pomocą odpowiedniego przyciusku.
#### Ustawienie programu względem nadania odpowiednich wartości dla urządzenia
>Za pomocą narzędzia jestesmy wstanie ustawić czas interwału działania pompy.
>W skłąd interwału wchodzi czas pracy i czas przerwy urządzenia
>Oba te czasy można ustawić w przystosowanych do tego polach ( Task time oraz Break time ) suma obu wartości daje czas interwału.