﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using Microsoft.Win32;

namespace SerialPortSender
{

    public partial class Form1 : Form
    {
        SerialPort _com_port = new SerialPort();
        public Form1()
        {
            InitializeComponent();
            InitPortList();
        }

        private void InitPortList()
        {
            // initializacja portów COM w liście porów 
            int nPorts = 0;
            string[] ports = SerialPort.GetPortNames();
            foreach (string port in ports)
            {
                comboBox1.Items.Add(port);
                nPorts++;
            }

            output_area.SelectionColor = Color.Chocolate;
            if (nPorts > 0) output_area.AppendText("Searching complete...\rSelect a correct port\r");
            else output_area.AppendText("No ports found...\r");
        }


        private void button1_Click(object sender, EventArgs e)
        {
            // usuwanie okienka 
            Close();
        }

        private string TimeConverter(string sArray) // uzupełniamy stringową array zerami jeżeli to konieczne 
        {
            int x = Int32.Parse(sArray);
            x = x * 10;

            string presArray = x.ToString();

            if (5 - presArray.Length == 0)
            {
                return presArray;
            }
            else
            {
                string ss = string.Empty;
                for (int i = 5 - presArray.Length; i > 0; i--)
                {
                    ss += "0";
                }

                ss += presArray;

                return ss;
            }
        }

        private void button2_Click(object sender, EventArgs e) // programowanie układu sterowania pompki 
        {

            if (comboBox1.Text == "")
            {
                output_area.SelectionColor = Color.Red;
                output_area.AppendText("Please select COM Port first...\r");
                output_area.ScrollToCaret();
            }
            else if (time_a.Text == "" || time_b.Text == "")
            {
                output_area.SelectionColor = Color.Red;
                output_area.AppendText("Type pice of informations...\r");
                output_area.ScrollToCaret();
            }
            else
            {
                // wysyłanie bitów do podanego portu 
                string com_name = comboBox1.Text;
                byte[] numb;
                byte[] output_message = new byte[5];
                string order = "W";

                // convertowanie i uzupełnienie stringów brakującymi zerami 
                order += TimeConverter(((Convert.ToInt16(time_a.Text) + Convert.ToInt16(time_b.Text)) * 2).ToString());
                order += TimeConverter((Convert.ToInt16(time_b.Text) * 2).ToString());

                order += '\r';

                numb = Encoding.Default.GetBytes(order.Substring(0, order.Length));

                try
                {
                    _com_port.PortName = com_name;
                    _com_port.BaudRate = 19200;
                    _com_port.Parity = Parity.None;
                    _com_port.StopBits = StopBits.One;
                    _com_port.DataBits = 8;
                    _com_port.Handshake = Handshake.None;
                    _com_port.RtsEnable = true;
                    _com_port.ReadTimeout = 500;

                    _com_port.Open();
                    _com_port.Write(numb, 0, numb.Length);
                    output_area.AppendText("Sending properties...\r");
                    output_area.ScrollToCaret();
                    for (int i = 0; i < 200000000; i++) { }
                    _com_port.Read(output_message, 0, output_message.Length);
                    //var value = output_message.GetValue(0);
                    output_area.SelectionColor = Color.Blue;
                    output_area.AppendText(ConvertOutMessage(output_message));
                    output_area.AppendText("\r");
                }
                catch (Exception err)
                {
                    output_area.SelectionColor = Color.Red;
                    output_area.AppendText(err.Message + "\r");
                    output_area.ScrollToCaret();
                }

                _com_port.Close();
            }
        }

        private void detect_btn_Click(object sender, EventArgs e)
        {

            output_area.SelectionColor = Color.Chocolate;
            output_area.AppendText("Searching for COM Ports...\r");
            comboBox1.Items.Clear();
            comboBox1.Text = "";
            InitPortList();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("www.mes.com.pl");
        }

        private void button3_Click(object sender, EventArgs e)
        {

            if (comboBox1.Text == "")
            {
                output_area.SelectionColor = Color.Red;
                output_area.AppendText("Select a COM port first...\r");
                output_area.ScrollToCaret();
            }
            else if (time_a.Text == "" || time_b.Text == "")
            {
                output_area.SelectionColor = Color.Red;
                output_area.AppendText("Type pice of informations...\r");
                output_area.ScrollToCaret();
            }
            else
            {
                string com_name = comboBox1.Text;
                byte[] output_message = new byte[5];
                byte[] byte_time_a = { 0x53, 0x0D };

                try
                {
                    _com_port.PortName = com_name;
                    _com_port.BaudRate = 19200;
                    _com_port.Parity = Parity.None;
                    _com_port.StopBits = StopBits.One;
                    _com_port.DataBits = 8;
                    _com_port.Handshake = Handshake.None;
                    _com_port.RtsEnable = true;
                    _com_port.ReadTimeout = 500;

                    _com_port.Open();
                    _com_port.Write(byte_time_a, 0, byte_time_a.Length);
                    output_area.AppendText("Propertie Time A sended...\r");
                    output_area.ScrollToCaret();
                    for (int i = 0; i < 200000000; i++) { }
                    _com_port.Read(output_message, 0, output_message.Length);
                    //string out_text = Encoding.ASCII.GetString(output_message);
                    output_area.SelectionColor = Color.Blue;
                    output_area.AppendText(ConvertOutMessage(output_message));
                    output_area.AppendText("\r");
                }
                catch (Exception err)
                {
                    output_area.SelectionColor = Color.Red;
                    output_area.AppendText(err.Message+"\r");
                    output_area.ScrollToCaret();
                }

                _com_port.Close();
            }
        }
        private string ConvertOutMessage(byte[] hexString) // formatowanie powrotnej informacji z urządzenia
        {
            string ascii = string.Empty;
            try
            {
                int outnn;
                int nnn;
                int nn;
                string chechss = string.Empty; 

                chechss += hexString.GetValue(0);
                if (chechss == "82")
                {
                    ascii += "R ";
                }
                else if (chechss == "83")
                {
                    ascii += "Apply complete ";
                    return ascii;
                }
                else if (chechss == "69")
                {
                    ascii += "Error!";
                    return ascii;
                }
                else if (chechss == "87")
                {
                    ascii += "W ";
                }


                ascii += " T1: ";
                string ss = hexString.GetValue(1).ToString();
                nn = Int32.Parse(ss);
               
                ss = hexString.GetValue(2).ToString();
                nnn = Int32.Parse(ss);
                outnn = nn * 16 * 16 + nnn;
                Variables.time_1 = outnn;
                ascii += outnn;
                ascii += ", T2: ";
                ss = hexString.GetValue(3).ToString();
                nn = Int32.Parse(ss);
                ss = hexString.GetValue(4).ToString();
                nnn = Int32.Parse(ss);
                outnn = nn * 16 * 16 + nnn;
                Variables.time_2 = outnn;
                ascii += outnn;

                return ascii;
            }
            catch (Exception ex)
            {
                output_area.SelectionColor = Color.Red;
                output_area.AppendText(ex.Message+"\r");
                output_area.ScrollToCaret();
                return ascii;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            
            if (comboBox1.Text == "")
            {
                output_area.SelectionColor = Color.Red;
                output_area.AppendText("Select a COM port first...\r");
                output_area.ScrollToCaret();
            }
            else
            {
                string com_name = comboBox1.Text;
                byte[] output_message = new byte[5];
                byte[] byte_time_a = { 0x52, 0x0D };

                try
                {
                    _com_port.PortName = com_name;
                    _com_port.BaudRate = 19200;
                    _com_port.Parity = Parity.None;
                    _com_port.StopBits = StopBits.One;
                    _com_port.DataBits = 8;
                    _com_port.Handshake = Handshake.None;
                    _com_port.RtsEnable = true;
                    _com_port.ReadTimeout = 500;

                    _com_port.Open();
                    _com_port.Write(byte_time_a, 0, byte_time_a.Length);
                    output_area.AppendText("Propertie Time A sended...\r");
                    output_area.ScrollToCaret();
                    for (int i = 0; i < 200000000; i++) { }
                    _com_port.Read(output_message, 0, output_message.Length);
                    string out_text = Encoding.ASCII.GetString(output_message);
                    output_area.SelectionColor = Color.Blue;
                    string deviceResponds = ConvertOutMessage(output_message);
                    // time_a.Text =  deviceResponds.Substring(1, 1);
                    // time_b.Text = deviceResponds.Substring(2, 1);
                    time_a.Text = ((Variables.time_1 - Variables.time_2) / 20).ToString();
                    time_b.Text = (Variables.time_2 / 20).ToString();
                    output_area.AppendText(deviceResponds);
                    output_area.AppendText("\r");
                }
                catch (Exception err)
                {
                    output_area.SelectionColor = Color.Red;
                    output_area.AppendText(err.Message + "\r");
                    output_area.ScrollToCaret();
                }

                _com_port.Close();
            }
        }
    }
}
